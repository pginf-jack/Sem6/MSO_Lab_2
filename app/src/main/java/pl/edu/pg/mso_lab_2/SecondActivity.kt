package pl.edu.pg.mso_lab_2

import android.content.Intent
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.ImageView
import android.widget.TextView

class SecondActivity : AppCompatActivity() {
    val textView: TextView by lazy { findViewById(R.id.second_text) }
    val cameraImage: ImageView by lazy { findViewById(R.id.camera_image) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val text = intent.getStringExtra("text")
        textView.text = text
    }

    override fun onBackPressed() {
        val resultIntent = Intent()
        resultIntent.putExtra("text", "output text from second activity")
        setResult(RESULT_OK, resultIntent)
        finish()
    }

    fun openCamera(view: View) {
        val imageCaptureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(imageCaptureIntent, 616)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 616 && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            if (imageBitmap != null) {
                cameraImage.setImageBitmap(imageBitmap)
            }
        }
    }
}