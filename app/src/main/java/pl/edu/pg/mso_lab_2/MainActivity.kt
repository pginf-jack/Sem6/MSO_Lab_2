package pl.edu.pg.mso_lab_2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    val textView: TextView by lazy { findViewById(R.id.main_text) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun switchToSecondActivity(view: View) {
        val secondActivityIntent = Intent(this, SecondActivity::class.java)
        secondActivityIntent.putExtra("text", "input text from main activity")
        startActivityForResult(secondActivityIntent, 999)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 999) {
            if (resultCode == RESULT_OK) {
                val text = data?.getStringExtra("text")
                textView.text = text
            }
        }
    }
}